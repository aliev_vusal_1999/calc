﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;

namespace WpfApp1
{
    public class Parser
    {
        public char[] strchar;
        public char c;
        public int i = 0;
        public int kol = 0;
        public double result = 0;
        public void Set(char x)
        {
            // i++;
            c = x;
        }
        public double ProcC()
        {
            double x = 0;
            while (c >= '0' && c <= '9')
            {
                //if (c == '.')
                //    c = ',';
                x *= 10;
                x += c - '0';
                if (kol == i)
                    break;
                Set(strchar[i++]);
            }
            return x;
        }
        public double ProcE()
        {
            double x = ProcT();
            while (c == '+' || c == '-')
            {
                char p = c;
                Set(strchar[i++]);
                if (p == '+')
                    x += ProcT();
                else
                    x -= ProcT();
            }
            return x;
        }
        public double ProcT()
        {
            double x = ProcM();
            while (c == '*' || c == '/')
            {
                char p = c;
                Set(strchar[i++]);
                if (p == '*')
                    x *= ProcM();
                else
                    x /= ProcM();
            }
            return x;
        }
        public double ProcM()
        {
            double x = 0;

            if (c == '(')
            {
                Set(strchar[i++]);
                x = ProcE();
                if (c != ')')
                    Console.WriteLine("Error");
                // Error("\')\' missing.", 0);
                Set(strchar[i++]);

            }
            else if (c == '-')
            {
                Set(strchar[i++]);
                x = -ProcM();
            }
            else if (c >= '0' && c <= '9')
                x = ProcC();
            //  else
            // Console.WriteLine("Error");
            //сделать унарный минус
            return x;
        }

        public void ProcS()
        {
            result = ProcE();
            //Set(strchar[i++]);
        }

        public double Parse(string str)
        {
            strchar = str.ToCharArray();
            kol = str.Length;
            Set(strchar[i++]);
            ProcS();

            return result;
        }
    }

    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            if (parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
    class ViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        //List<string> coll = new List<string>();

        public ObservableCollection<string> Memory { get; }
        public ViewModel()
        {
            _buttonCommand = new RelayCommand<string>(x => Result += x);

            //MyCollection = new ObservableCollection<string>();
            //AddToCollection = new RelayCommand(() =>
            //{
            //    var newValue = MyCollection.Any() ? MyCollection.Where
            //})
            Memory = new ObservableCollection<string>();


        }

        public string _Result;

        public string Result
        {
            get { return _Result; }
            set { _Result = value; OnPropertyChanged(nameof(Result)); }
        }

        public string _SelectedMemory;

        public string SelectedMemory
        {
            get { return _SelectedMemory; }
            set { _SelectedMemory = value; OnPropertyChanged(nameof(SelectedMemory)); }
        }

        private ICommand _deleteCommand = new DeleteCommand();
        public ICommand DeleteCommand { get { return _deleteCommand; } }

        private ICommand _buttonCommand = new ButtonCommand();
        public ICommand ButtonCommand { get { return _buttonCommand; } }

        private ICommand _equallyCommand = new EquallyCommand();
        public ICommand EquallyCommand { get { return _equallyCommand; } }

        private ICommand _addMemCommand = new AddMemCommand();
        public ICommand AddMemCommand { get { return _addMemCommand; } }

        private ICommand _clearMemCommand = new ClearMemCommand();
        public ICommand ClearMemCommand { get { return _clearMemCommand; } }

        private ICommand _plusMemCommand = new PlusMemCommand();
        public ICommand PlusMemCommand { get { return _plusMemCommand; } }

        private ICommand _minusMemCommand = new MinusMemCommand();
        public ICommand MinusMemCommand { get { return _minusMemCommand; } }

        public string Error => throw new NotImplementedException();

        public string this[string columnName] => throw new NotImplementedException();



        private void OnPropertyChanged(string PropertyName)
        {
            if (PropertyName == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;



    }

    public class ButtonCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;
            viewModel.Result += parameter.ToString();
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
    public class EquallyCommand : ICommand 
    {
        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            Parser P = new Parser();

            double result = P.Parse(viewModel.Result);

            viewModel.Result = result.ToString();
        }

        public bool CanExecute(object parameter)
        {
            var viewModel = parameter as ViewModel;
            if (string.IsNullOrEmpty(viewModel?.Result))
                return false;
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
    public class DeleteCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;
            viewModel.Result = "";
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class AddMemCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            Parser P = new Parser();

            double result = P.Parse(viewModel.Result);

            viewModel.Result = result.ToString();

            

            viewModel.Memory.Add(viewModel.Result);
            viewModel.SelectedMemory = viewModel.Result;
        }

        public bool CanExecute(object parameter)
        {
            var viewModel = parameter as ViewModel;
            if (string.IsNullOrEmpty(viewModel?.Result))
                return false;
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class ClearMemCommand : ICommand
    {
       
        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;
            viewModel.SelectedMemory = "";
        }

        public bool CanExecute(object parameter)
        {
            //var viewModel = parameter as ViewModel;
            //if (string.IsNullOrEmpty(viewModel?.memory))
            //    return false;
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
    public class PlusMemCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;
           
            viewModel.Result += viewModel.SelectedMemory;
        }

        public bool CanExecute(object parameter)
        {
            //return true;
            //var viewModel = parameter as ViewModel;
            //if (string.IsNullOrEmpty(viewModel?.memory))
            //    return false;
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class MinusMemCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var viewModel = parameter as ViewModel;

            //Parser P = new Parser();

            //double result = P.Parse(viewModel.Result) - Convert.ToDouble(viewModel.memory);

            //viewModel.Result = result.ToString();
           // viewModel.Memory.Remove(viewModel.Result);
            viewModel.Memory.Remove(viewModel.SelectedMemory);

        }

        public bool CanExecute(object parameter)
        {
            //var viewModel = parameter as ViewModel;
            //if (string.IsNullOrEmpty(viewModel?.memory))
            //    return false;
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}
